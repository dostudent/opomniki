window.addEventListener('load', function() {
	//stran nalozena
		
	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");
		
		for (i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);
			
			if(cas == 0){
				opomnik.remove();
				alert("Opomnik!\n\nZadolžitev " + opomnik.querySelector(".naziv_opomnika").innerHTML + " je potekla!");
			}else{
				casovnik.innerHTML = cas - 1;
			}
		}
	}
	setInterval(posodobiOpomnike, 1000);

});

function prijava(){
	document.getElementById("uporabnik").innerHTML = document.querySelector("#uporabnisko_ime").value;
	document.getElementsByClassName("pokrivalo")[0].style.visibility = 'hidden';
}

function dodajOpomnik(){
	var opomnik = document.querySelector("#naziv_opomnika").value;
	var cas = document.querySelector("#cas_opomnika").value;
	document.querySelector("#naziv_opomnika").value = "";
	document.querySelector("#cas_opomnika").value = "";
	var noviOpomnik =  
	"<div class='opomnik senca rob'>" + 
	"<div class='naziv_opomnika'>" + opomnik + "</div>" +
	"<div class='cas_opomnika'> Opomnik čez <span>" + cas + "</span> sekund.</div>" +
	"</div>";
	document.getElementById("opomniki").innerHTML = document.getElementById("opomniki").innerHTML + noviOpomnik;
}